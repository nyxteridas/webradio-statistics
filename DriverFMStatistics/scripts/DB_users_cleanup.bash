#!/bin/bash
# Script for cleaning user sessions older two minutes and idle

echo "Cleaning user sessions!"
sudo -u postgres /usr/bin/psql -h 'HOST-OF-POSTGRESQL' -d 'DATABASE' -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'DATABASE' AND pid <> pg_backend_pid() AND state in ('idle', 'idle in transaction', 'idle in transaction (aborted)', 'disabled') AND ((application_name NOT LIKE '%pgAdmin%' AND state_change < current_timestamp - INTERVAL '2' MINUTE) OR (application_name LIKE '%pgAdmin%' AND state_change < current_timestamp - INTERVAL '15' MINUTE))"
echo "Cleaning completed!"
