#!/bin/bash
# Script for grouping statistics

# Definition of current date
now_month=$(date +"%B")
last_month=$(date --date="-1 month" +"%B")
now_year=$(date +"%Y")
last_year=$(date --date="-1 year" +"%Y")
dash="_"

#echo "last month $last_month"
#echo "last year $last_year"
#echo "current month $now_month"
#echo "current year $now_year"
#filename=$last_month$dash$now_year
#echo "$filename"

if [[ $now_month == "January" ]]; then 

finalname=$last_month$dash$last_year
#echo "last year $last_year \n"

else

finalname=$last_month$dash$now_year

fi

cd <PATH-TO-LOGS>

#echo "finalname: $finalname"

tar zcf "$finalname.tar.gz" "$finalname"
rm -r "$finalname"
