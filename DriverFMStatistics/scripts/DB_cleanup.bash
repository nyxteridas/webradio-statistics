#!/bin/bash
# Script for cleaning data older than a year

echo "Cleaning records older than a year!"
sudo -u postgres /usr/bin/psql -h 'HOST-OF-POSTGRESQL' -d 'DATABASE' -c "DELETE from statistics WHERE time_taken < NOW() - INTERVAL '365 days'"
echo "Cleaning completed!"

