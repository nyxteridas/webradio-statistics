#!/bin/bash
# Script for checking ratings

# add hostname
  GOOGLE="www.google.com"    

# no of ping request
  COUNT=3
  logfolder=<PATH-TO-LOGS>
  IP="192.192.192.1"
  PORT="1568"
  STATSPAGE="http://stats.somehost.somecountry/endpoint.php"
  HOST-OF-POSTGRESQL="HOST"
  DATABASE="DBNAME"

# Server up indication
  server_up=1

# Definition of current day and time
  now_date=$(date +"%d_%a")
  now_time=$(date +"%R")
  now_month=$(date +"%B_%Y")
  now_timestamp=$(date +"%Y-%m-%d %H:%M")

  if [ -d "$logfolder/$now_month" ]; then
      echo "File found."
    else
      mkdir "$logfolder/$now_month"
  fi

  if [ -f "$logfolder/$now_month/$now_date.csv" ]; then
    echo "File found."
  else
    touch "$logfolder/$now_month/$now_date.csv"
  fi
  
  final_output="$now_timestamp,"

  count=$(ping -c $COUNT $GOOGLE | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
  if [[ "$count" -eq 0 ]]; then	      	 
  # 100% failed
    echo "Dead"
    server_up=0	  
    listeners="0,"
    comment="No Internet Connection"
    final_output=$final_output$listeners$comment

    #insert to db
    sudo -u postgres /usr/bin/psql -h '$HOST-OF-POSTGRESQL' -d '$DATABASE' -c "INSERT INTO statistics (time_taken, listeners, comment) VALUES (to_timestamp('$now_timestamp', 'YYYY-MM-DD hh24:mi')::timestamp without time zone, 0, '$comment')"
  else
    echo "Internet connection is up"

    state=$(nmap -p $PORT -sT $IP | grep -c 'open')
    if [[ "$state" -eq 0 ]]; then  

    # 100% failed
      echo "Dead $IP"
      server_up=0		
      listeners="0,"
      comment="Streaming Server Down"
      final_output=$final_output$listeners$comment

      #insert to db
      sudo -u postgres /usr/bin/psql -h '$HOST-OF-POSTGRESQL' -d '$DATABASE' -c "INSERT INTO statistics (time_taken, listeners, comment) VALUES (to_timestamp('$now_timestamp', 'YYYY-MM-DD hh24:mi')::timestamp without time zone, 0, '$comment')"
    else
      echo "Streaming server is up"
      listeners=$(wget '$STATSPAGE' -q -O -  | grep -i '<div class=\"listeners\">' | awk -F '>' '{print $2}' | awk '{print $6}')
      comment=",' '"
      # echo $temp
      final_output=$final_output$listeners$comment

      #insert to db
      sudo -u postgres /usr/bin/psql -h '$HOST-OF-POSTGRESQL' -d '$DATABASE' -c "INSERT INTO statistics (time_taken, listeners, comment) VALUES (to_timestamp('$now_timestamp', 'YYYY-MM-DD hh24:mi')::timestamp without time zone, '$listeners', ' ')"
    fi
  fi

  echo $final_output >> "$logfolder/$now_month/$now_date.csv"
