-- Table: public.statistics

-- DROP TABLE public.statistics;

CREATE TABLE public.statistics
(
    id SERIAL,
    time_taken timestamp without time zone NOT NULL,
    listeners integer,
    comment character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT statistics_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.statistics
    OWNER to postgres;