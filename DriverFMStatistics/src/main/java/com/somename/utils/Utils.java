package com.somename.utils;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somename.bean.Statistics;

public class Utils {

	private static final Logger log = LoggerFactory.getLogger(Utils.class);
	
	public static JsonNode convertToJson(List<Statistics> stats) {	
			
		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = null;
		String json = "{ \"cols\":[{ \"label\":\"Date/Time\", \"type\":\"datetime\" }," + 
		              "{ \"label\":\"Listeners\", \"type\":\"number\" }], \"rows\":[";
		
		if (stats.size() != 0){
			for(Statistics st : stats){
				json += "{ \"c\":[{ \"v\":\"" + convertDateToString(st.getTime_taken()) + "\"}, { \"v\":" + st.getListeners() + "}]},";
			}
			json = json.substring(0, json.length()-1) + "]}";
			
			try{
				log.info("Converting String to Json");
				actualObj = mapper.readTree(json);
			}
			catch(JsonProcessingException e){
				log.info("JsonProcessingException while converting");
				return null;
			}
			catch(IOException e){
				log.info("IOException while converting");
				return null;
			}
		}
		else{
			try{
				log.info("ResultSet is empty. Return empty tree.");
				actualObj = mapper.readTree("{}");
			}
			catch(JsonProcessingException e){
				log.info("JsonProcessingException while converting");
				return null;
			}
			catch(IOException e){
				log.info("IOException while converting");
				return null;
			}
		}

		return actualObj;
	}	
	
	public static String convertDateToString(Date timeTaken) {
		
		String tempDate = timeTaken.toString();
		tempDate = tempDate.substring(0, tempDate.lastIndexOf("."));
		String[] tempDateTime = tempDate.split(" ");
		String[] temp = tempDateTime[0].split("-");
		tempDate = "";
		for (int i = 0; i < temp.length; i++){
			if (i == 1)
				tempDate += Integer.toString(Integer.parseInt(temp[i]) - 1) + ",";
			else
				tempDate += temp[i] + ",";
		}		
		tempDateTime[1] = tempDateTime[1].replaceAll(":", ",");
				
		return "Date(" + tempDate + tempDateTime[1] + ")";
		
	}
}
