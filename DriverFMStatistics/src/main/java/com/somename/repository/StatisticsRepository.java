package com.somename.repository;

import java.util.List;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.somename.bean.Statistics;

@Repository
public interface StatisticsRepository extends CrudRepository<Statistics, Integer> {
	
	@Query(value = "SELECT * FROM statistics WHERE time_taken > ?1 AND time_taken < ?2 AND comment = ' '", nativeQuery = true)
	List<Statistics> findByTimeTakenGreaterThanAndTimeTakenLessThan(Date dtGrt, Date dtLs);
	
	@Query(value = "SELECT ROW_NUMBER() OVER () AS \"id\", md5(random()\\:\\:varchar(255)) AS \"comment\", * FROM (SELECT date_trunc('day', time_taken) AS \"time_taken\" , ROUND (avg(listeners),0) AS \"listeners\" FROM statistics WHERE comment = ' ' GROUP BY 1 ORDER BY 1 DESC LIMIT 7) AS tabl", nativeQuery = true)
	List<Statistics> findDailyAverageForCurrentMonth();
	
	@Query(value = "SELECT ROW_NUMBER() OVER () AS \"id\", md5(random()\\:\\:varchar(255)) AS \"comment\", * FROM (SELECT date_trunc('month', time_taken) AS \"time_taken\", ROUND (avg(listeners),0) AS \"listeners\" FROM statistics WHERE comment = ' ' GROUP BY 1 ORDER BY 1 DESC LIMIT 12) AS tabl", nativeQuery = true)
	List<Statistics> findMonthlyAverageForCurrentYear();
}
