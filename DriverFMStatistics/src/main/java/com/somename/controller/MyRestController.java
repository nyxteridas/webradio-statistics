package com.somename.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somename.repository.StatisticsRepository;
import com.somename.utils.Utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.QueryParam;

@RestController
public class MyRestController {

	@Autowired
	StatisticsRepository statisticsRepository;

	private static final Logger log = LoggerFactory.getLogger(MyRestController.class);

	@GetMapping("/stats/compare")
	public JsonNode comparisonDates(@QueryParam("greater") String greater, @QueryParam("less") String less) {

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date greaterDate = new Date();
		Date lessDate = new Date();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			greaterDate = formatter.parse(greater);
			log.info("Starting Date: " + greaterDate);
			lessDate = formatter.parse(less);
			log.info("Ending Date: " + lessDate);
		} catch (ParseException e) {
			try{
				log.info("Exception while converting date ranges. Returning empty tree.");
				return mapper.readTree("{}");
			}
			catch(JsonProcessingException ex){
				log.info("JsonProcessingException while converting date ranges");
				ex.printStackTrace();
			}
			catch(IOException ex){
				log.info("IOException while converting date ranges");
				ex.printStackTrace();
			}
		}
		
		return Utils.convertToJson(statisticsRepository.findByTimeTakenGreaterThanAndTimeTakenLessThan(greaterDate, lessDate));
		
	}	
	
	@GetMapping("/stats/monthly")
	public JsonNode monthlyStats() {
				
		return Utils.convertToJson(statisticsRepository.findMonthlyAverageForCurrentYear());
		
	}	
	
	@GetMapping("/stats/daily")
	public JsonNode dailyStats() {
				
		return Utils.convertToJson(statisticsRepository.findDailyAverageForCurrentMonth());
		
	}	
}