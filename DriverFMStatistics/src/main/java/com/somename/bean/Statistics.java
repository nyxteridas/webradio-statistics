package com.somename.bean;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Table(name="statistics")
public class Statistics{
	
	@Id
    @SequenceGenerator(name="statistics_id_seq",
                       sequenceName="statistics_id_seq",
                       allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
                    generator="statistics_id_seq")
	@Column(name = "id", updatable=false)
    private Integer id;
	
	@Column(name = "time_taken", columnDefinition= "TIMESTAMP WITHOUT TIME ZONE")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeTaken;
	
	@Column(name = "listeners")
	private Integer listeners;
	
	@Column(name = "comment")
	private String comment;
	
	public Statistics(){}
	public Statistics(Integer id, @NotNull Date time_taken, Integer listeners, String comment) {
		super();
		this.id = id;
		this.timeTaken = time_taken;
		this.listeners = listeners;
		this.comment = comment;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getTime_taken() {
		return timeTaken;
	}
	public void setTime_taken(Date time_taken) {
		this.timeTaken = time_taken;
	}
	public Integer getListeners() {
		return listeners;
	}
	public void setListeners(Integer listeners) {
		this.listeners = listeners;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listeners == null) ? 0 : listeners.hashCode());
		result = prime * result + ((timeTaken == null) ? 0 : timeTaken.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statistics other = (Statistics) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listeners == null) {
			if (other.listeners != null)
				return false;
		} else if (!listeners.equals(other.listeners))
			return false;
		if (timeTaken == null) {
			if (other.timeTaken != null)
				return false;
		} else if (!timeTaken.equals(other.timeTaken))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Statistics [id=" + id + ", time_taken=" + timeTaken + ", listeners=" + listeners + ", comment="
				+ comment + "]";
	}	
}
