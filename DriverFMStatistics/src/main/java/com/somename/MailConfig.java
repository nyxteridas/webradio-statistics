package com.somename;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {
	
	@Value("${email.protocol}")
    private String protocol;
	
    @Value("${email.host}")
    private String host;
    
    @Value("${email.port}")
    private int port;
        
    @Value("${email.smtp.auth}")
    private boolean auth;
    
    @Value("${email.smtp.starttls.enable}")
    private boolean starttls;
    
    @Value("${email.smtp.starttls.required}")
    private boolean startlls_required;
            
    @Value("${email.from}")
    private String from;
    
    @Value("${email.username}")
    private String username;
    
    @Value("${email.password}")
    private String password;

	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", auth);
        mailProperties.put("mail.smtp.starttls.enable", starttls);
        mailProperties.put("mail.smtp.starttls.required", startlls_required);
        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        mailProperties.put("mail.smtp.ssl.trust", "*");
        mailProperties.put("mail.smtp.socketFactory.port", port);
        mailProperties.put("mail.smtp.socketFactory.fallback", "false");

        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setProtocol(protocol);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
		
		return mailSender;
	}
}
