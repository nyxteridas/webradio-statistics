## Webradio-statistics v1.0

An integrated platform in order to track and visualize listening statistics for a webradio, almost in real-time.
The implementation includes a bash script which collects the number of listeners periodically and inserts the necessary elements into a PostgreSQL database. Visualization of statistics is done by using Bootstrap and GoogleCharts on the frontend, and a Java Spring Boot application on the backend. The backend uses, among others, JPA, Spring security, Thymeleaf, Jersey, Mail. The development of the frontend was undertaken by Panagiotis Loukouzas, and the backend by me.

---

## Database

In our case we used PostgreSQL solution. You can use whichever DB is suitable for you, but keep in mind that you will need to change the solution in order to work with your choices.

For this solution we are going to need only one table named "statistics". 
That table will contain 4 columns:

1. id (autoincrement-SERIAL)
Our primary key.
2. time_taken (timestamp without timezone)
This field holds the time in which the measurement was taken.
3. listeners (integer)
This field holds the number of listeners
4. comment (varchar 255)
This field is applicable only in cases an error occured during the retrieval of measurement.

You can find the create query in the folder "SQL"

---

## BASH Scripts accompanying the solution

** stats.sh **
This is the most important script. From this script we read the number of listeners and insert it in our DB.
You will need to edit parameters: 

1. logfolder (absolute path to the folder in which you want to store the logs)
2. IP (IP of the streaming server)
3. PORT (Port of the streaming server)
4. STATSPAGE (Statistics page holding the total number of users)
5. HOST-OF-POSTGRESQL (Host of postgresql)
6. DATABASE (database name)

The script works as described below:

1. First of all a check is made that the folder for current month, and the file for current day is already created. If not, it will be.
2. The second step is to start creating the record that will be inserted in the DB. This record will be created step-by-step. The first thing to be inserted, is the current timestamp.
3. The general connectivity to the internet is then tested. If no connectivity exists, then the number of listeners is set to 0, and the comment to "No Internet Connection". This record will be output to our log file, and also inserted in our DB.
4. If the connection to internet is up, we nmap our streaming server to ensure that the station is up and running. If not, number of listeners is set to 0 and comment to "Streming Server Down". This record will be output to our log file, and also inserted in our DB.
5. If also the streaming server is up, we are reading via wget the statistics page given. Then with the help of grep and awk we are isolate from the page's code only the total number of listeners. Then we set this number to the listeners number and we leave comment empty. This record will be output to our log file, and also inserted in our DB.
6. The output log is kept in csv format for easier restore in case DB fails.

** grouping.sh **
Used for archiving logs of previous month.
In order to have this script working, you need to change "<PATH-TO-LOGS>" with the path you have stored your logs (setup in stats.sh)
This script will work better if you have it run at first day of the month, before the first measurement is taken for the new month.

** DB_cleanup.bash **
Used for cleaning DB from records older than a year. You can put this script running once a month, or as frequently as you wish. 
You can also edit the range of dates you wish to remove.
In order to have this script working, you need to review the whole command in it, to ensure that you have put your settings in.

** DB_users_cleanup.bash **
Used for cleaning user sessions older than two minutes and idle, or older than fifteen minutes and idle in case we are talking for pgAdmin application.
In order to have this script working, you need to review the whole command in it, to ensure that you have put your settings in.

---

## Backend solution

There is no need to get in the solution class-by-class. We will elaborate a bit in the important parts.
This is a spring boot application using JPA, spring security, Thymeleaf, Jersey and Mail.
Below is the hierarchy of the backend solution:

   |-/                                                                                                                            
    |-src                                                                                                                        
      |-main
        |--java.com.somename
          |---bean
            |----Statistics.java ** Our model **
          |---controller
            |----MyRestController.java
          |---repository
            |----StatisticsRepository.java ** our DAO **
          |---utils
            |----Utils.java
          |---Application.java ** Starts our spring boot application **
          |---DataSourceConfig.java ** In this class we are setting up our Data Source **
          |---MailConfig.java ** In this class we are setting up our mail client **
          |---MvcConfig.java ** Map our endpoints with our views under resources/templates **
          |---ScheduledTasks.java ** A simple cron-like job checking if our datasource is up. If not, an email is sent to the specified in application.properties email **
          |---WebSecurityConfig.java ** Setting up the security settings for our application **
        |--resources
          |---templates ** it will be discussed on frontend part **
          |---application.properties
          |---banner.txt

The parts we are going to discuss are:

1. **The application.properties file**
2. **The controller of the application**
3. **The utils class**

DAO, model and configuration classes are quite straight forward, so there is no need to spend time on them.

## The application.properties file

In this file we are putting some general properties that the application needs in order to run correctly. 

**"spring.datasource.url"**, **"spring.datasource.username"** and **"spring.datasource.password"**. Those three parameters are needed in order to connect to the database

**"entitymanager.packagesToScan"** is needed from hibernate in order to know in which package it can find the model classes.

**"logging.file"** the path along with the filename of your logfile.

**"security.user.name"** and **"security.user.password"** are needed from spring security. Those are the credentials you are going to need in order to login in the website. Of course there are several other ways to make this working, but in our case we needed something easy. You can even encrypt those credentials in the application properties file, or even the whole file.

**"server.address"**, **"server.port"** and **"server.contextPath"**. 
Those info are regarding tomcat. You are setting the server's address, the port in which tomcat will run and also the base endpoint of the application.

**"email.username"**, **"email.password"**, **"email.from"** and **"email.to"**. 
Those settings are regarding the mailing service.
In our case we have implemented a mailing service using gmail's smtp server. If you wish to use another SMTP server, please review all the settings under #gmail.

**"banner.location"** is the location of a banner which appears when the application starts up.


## The controller of the application

The controller of the application handles the requests coming on its endpoints.
As you can see in the code, three endpoints exist:

1. **/stats/compare** - based on two query parameters (timestamps) given in the url, returns the measurements taken between those two timestamps.
2. **/stats/monthly** - returns the average number of listeners per month, for the past one year. No parameters needed
3. **/stats/daily** - returns the average number of listeners per day, for the past one week. No parameters needed

Below you can see three examples, one for each endpoint, including also the domain and the endpoint you set on application.properties file.

1. http://yourserver-domain/<ENDPOINT>/stats/compare?greater=2018-03-24 00:00:00&less=2018-03-24 20:00:00
2. http://yourserver-domain/<ENDPOINT>/stats/monthly
3. http://yourserver-domain/<ENDPOINT>/stats/daily

The response of those requests is in JSON format, but since GoogleCharts need a very specific form of JSON, Controller asks from Utils class (method convertToJson) to transform the output of the DAO in the desired JSON format.
Then it returns this JSON to frontend.

Please find in the base folder an example of such JSON.


## The utils class

This class contains two methods only: 

1. **convertToJson**
2. **convertDateToString**.

The first method is the one we discussed in the controller part.
It takes as input a list of Statistics objects and parses them in a way that can be directly used from GoogleCharts in the frontend. 
In this way, the browser will not need to do any editing in the response it gets from the server, so we will gain resources.
The way a JSON for GoogleCharts is structured, is a little bit complicated and out of scope for this documentation. You can find many references in the web regarding this.

The second method is used from "convertToJson" method in order to convert a Date variable to the format "Date(2018,4,15,19,20,00)" which is needed again from the GoogleCharts.

---

## Frontend solution

Below is the hierarchy of the frontend solution:

   |-/                                                                                                                            
    |-src                                                                                                                        
      |-main
        |--resources
          |---templates
            |----home.html
            |----login.html
            
So two html files is the only thing we want for our frontend.

The first one is the **login.html**.
This is the login page we need for our application.
If you have correctly set all the parameters which we have mentioned till now, this page will work without problems. So there is no need for updates there, instead if you need to make textual updates.

The second one is the **home.html**.
This is the home page, which is also our main page.
In the source code of this, you will need to update all the endpoints, wherever they are mentioned (only in the Javascript part).
We encourage you not to change anything else in the Javascript part, except if you are competent with that.

Main page gives you the view of three seperate charts:

1. You can select a starting date/time and an ending date/time. For that range you will see all the statistics gathered in a Combo Chart.
2. A bar chart which will show the average number of listeners per day, for the last 7 days.
3. A bar chart which will show the average number of listeners per month, for the last 1 year.

